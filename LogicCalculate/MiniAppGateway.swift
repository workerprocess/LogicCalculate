//
//  MiniAppGateway.swift
//  LogicCalculate
//
//  Created by ekachai mankongtongjaroen on 10/10/2566 BE.
//

import Foundation
import SwiftUI
public protocol MiniAppGateway{
    init()
    func OpenView(openView:Binding<Bool>)->AnyView
    func OpenView2(openView:Binding<Bool>,title:String)->AnyView
}
