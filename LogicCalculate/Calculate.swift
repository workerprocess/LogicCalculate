//
//  Calculate.swift
//  LogicCalculate
//
//  Created by ekachai mankongtongjaroen on 10/10/2566 BE.
//

import Foundation
public class Calculate{
    public static func plus(n1:Int, n2:Int)-> Int{
        return n1 + n2
    }
    
    public static func minus(n1:Int, n2:Int)->Int{
        return n1 - n2
    }
    
}
