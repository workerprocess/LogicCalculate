#!/bin/sh

#  buildframework.sh
#
#  Created by ekachai mankongtongjaroen on 10/10/2566 BE.
#  
set -x
set -e

moduleName="LogicCalculate"
iphoneosArchiveDirectoryPath="/$moduleName-iphoneos.xcarchive"
iphoneosArchiveDirectory="$( pwd; )$iphoneosArchiveDirectoryPath"

iphoneosArchiveDirectoryPath="/$moduleName-iphonesimulator.xcarchive"
iphoneosSimulatorDirectory="$( pwd; )$iphoneosArchiveDirectoryPath"

outputDirectory="$( pwd; )/$moduleName.xcframework"

## Cleanup
rm -rf $iphoneosArchiveDirectory
rm -rf $iphoneosSimulatorDirectory
rm -rf outputDirectory

# Archive
xcodebuild archive -scheme $moduleName \
     -archivePath $iphoneosArchiveDirectory \
     -sdk iphoneos \
     SKIP_INSTALL=NO BUILD_LIBRARIES_FOR_DISTRIBUTION=YES

xcodebuild archive -scheme $moduleName \
     -archivePath $iphoneosSimulatorDirectory \
     -sdk iphonesimulator \
     SKIP_INSTALL=NO BUILD_LIBRARIES_FOR_DISTRIBUTION=YES

## XCFramework
xcodebuild -create-xcframework \
    -framework "$iphoneosArchiveDirectory/Products/Library/Frameworks/$moduleName.framework" \
    -framework "$iphoneosSimulatorDirectory/Products/Library/Frameworks/$moduleName.framework" \
    -output $outputDirectory

## Cleanup
rm -rf $iphoneosArchiveDirectory
rm -rf $iphoneosSimulatorDirectory

## Copy
cd ..
mkdir "xcframework/$moduleName-Distribute"
cp -R "$( pwd; )/$moduleName/$moduleName.xcframework" "$( pwd; )/xcframework/$moduleName-Distribute"

cat > "$( pwd; )/xcframework/$moduleName-Distribute/Package.swift" <<-EOF
// swift-tools-version:5.7
import Foundation
import PackageDescription
let package = Package(
    name: "$moduleName",
    platforms: [
        .iOS(.v16)
    ],
    products: [
        .library(
            name: "$moduleName",
            targets: ["$moduleName"])
    ],
    targets: [
        .binaryTarget(
            name: "$moduleName",
            path: "$moduleName.xcframework")
    ])
EOF

